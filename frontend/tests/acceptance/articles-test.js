import Ember from 'ember';
import { module, test } from 'qunit';
import startApp from '../helpers/start-app';

var application;
var originalConfirm;
var confirmCalledWith;

function defineFixturesFor(name, fixtures) {
  var modelClass = application.__container__.lookupFactory('model:' + name);
  modelClass.FIXTURES = fixtures;
}

module('Acceptance: Article', {
  beforeEach: function() {
    application = startApp();
    defineFixturesFor('article', []);
    originalConfirm = window.confirm;
    window.confirm = function() {
      confirmCalledWith = [].slice.call(arguments);
      return true;
    };
  },
  afterEach: function() {
    Ember.run(application, 'destroy');
    window.confirm = originalConfirm;
    confirmCalledWith = null;
  }
});

test('visiting /articles without data', function(assert) {
  visit('/articles');

  andThen(function() {
    assert.equal(currentPath(), 'articles.index');
    assert.equal(find('#blankslate').text().trim(), 'No Articles found');
  });
});

test('visiting /articles with data', function(assert) {
  defineFixturesFor('article', [{ id: 1, title: 'MyString', author: 'MyString', body: 'MyString' }]);
  visit('/articles');

  andThen(function() {
    assert.equal(currentPath(), 'articles.index');
    assert.equal(find('#blankslate').length, 0);
    assert.equal(find('table tbody tr').length, 1);
  });
});

test('create a new article', function(assert) {
  visit('/articles');
  click('a:contains(New Article)');

  andThen(function() {
    assert.equal(currentPath(), 'articles.new');

    fillIn('label:contains(Title) input', 'MyString');
    fillIn('label:contains(Author) input', 'MyString');
    fillIn('label:contains(Body) input', 'MyString');

    click('input:submit');
  });

  andThen(function() {
    assert.equal(find('#blankslate').length, 0);
    assert.equal(find('table tbody tr').length, 1);
  });
});

test('update an existing article', function(assert) {
  defineFixturesFor('article', [{ id: 1 }]);
  visit('/articles');
  click('a:contains(Edit)');

  andThen(function() {
    assert.equal(currentPath(), 'articles.edit');

    fillIn('label:contains(Title) input', 'MyString');
    fillIn('label:contains(Author) input', 'MyString');
    fillIn('label:contains(Body) input', 'MyString');

    click('input:submit');
  });

  andThen(function() {
    assert.equal(find('#blankslate').length, 0);
    assert.equal(find('table tbody tr').length, 1);
  });
});

test('show an existing article', function(assert) {
  defineFixturesFor('article', [{ id: 1, title: 'MyString', author: 'MyString', body: 'MyString' }]);
  visit('/articles');
  click('a:contains(Show)');

  andThen(function() {
    assert.equal(currentPath(), 'articles.show');

    assert.equal(find('p strong:contains(Title:)').next().text(), 'MyString');
    assert.equal(find('p strong:contains(Author:)').next().text(), 'MyString');
    assert.equal(find('p strong:contains(Body:)').next().text(), 'MyString');
  });
});

test('delete a article', function(assert) {
  defineFixturesFor('article', [{ id: 1, title: 'MyString', author: 'MyString', body: 'MyString' }]);
  visit('/articles');
  click('a:contains(Remove)');

  andThen(function() {
    assert.equal(currentPath(), 'articles.index');
    assert.deepEqual(confirmCalledWith, ['Are you sure?']);
    assert.equal(find('#blankslate').length, 1);
  });
});
