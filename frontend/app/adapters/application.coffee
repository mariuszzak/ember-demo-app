`import DS from "ember-data"`
`import ENV from '../config/environment'`

ApplicationAdapter = DS.ActiveModelAdapter.extend
  namespace: 'api',
#  host: ENV.APP.backendURL
  headers:
    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')


`export default ApplicationAdapter`
