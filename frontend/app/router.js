import Ember from "ember";
import config from "./config/environment";

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource("articles", function() {
    this.route("new");

    this.route("edit", {
      path: ":article_id/edit"
    });

    this.route("show", {
      path: ":article_id"
    });
  });
});

export default Router;