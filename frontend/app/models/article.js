import DS from 'ember-data';

var Article = DS.Model.extend({
  title: DS.attr('string'),
  author: DS.attr('string'),
  body: DS.attr()
});

Article.reopenClass({
  FIXTURES: []
});

export default Article;
